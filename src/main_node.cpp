#include "ros/ros.h"
#include "std_msgs/String.h" //std_msgs.h 
#include "std_msgs/UInt16.h"
#include "speech_jv/target.h"

// creat parameter for global using

void chatterCallback(const speech_jv::target::ConstPtr& msg)
{
  ROS_INFO("I heard a target : [%d] [%s] [%s] [%s]", msg->q ,msg->place.c_str(),msg->object.c_str(),msg->human.c_str());
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "main_node");
  ros::NodeHandle n;

  ros::Subscriber sub_target = n.subscribe("Target", 1000, chatterCallback);

  ros::spin();

  return 0;
}