#include "ros/ros.h"
#include <sensor_msgs/Joy.h>
#include <std_msgs/Float64.h>


void chatterCallback(const sensor_msgs::Joy::ConstPtr& msg){
	
	
}

int main(int argc, char **argv){

	ros::init(argc, argv, "node_main");
	ros::NodeHandle n;

	ros::Subscriber sub = n.subscribe("joy", 1000, chatterCallback);
	ros::Publisher chatter_pub = n.advertise<std_msgs::Float64>("/pan_controller/command", 1000);
	ros::spin();

	return 0;
}
