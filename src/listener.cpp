#include "ros/ros.h"
#include "speech_jv/num.h"

// creat parameter for global using

void chatterCallback(const speech_jv::num::ConstPtr& msg)
{
  ROS_INFO("I heard a target : [%d]", msg->x);
}

int main(int argc, char **argv)
{
  
  ros::init(argc, argv, "listener");
  ros::NodeHandle n;

  ros::Subscriber sub_target = n.subscribe("custom_chatter", 1000, chatterCallback);

  ros::spin();

  return 0;
}