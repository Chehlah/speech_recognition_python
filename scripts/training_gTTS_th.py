#!/usr/bin/env python
import rospy

from gtts import gTTS
from pygame import mixer
from playsound import playsound

import xml.etree.cElementTree as ET

rospy.init_node('traning_kid_jv', anonymous=True)
mixer.init()
tree = ET.ElementTree(file ='sound_kid_jv.xml')

savef = "say2"

for elem in tree.iter(savef):
	text = elem.attrib['str']
# gTTS
# text_in = text.decode('utf-8')
tts = gTTS(text, lang='th')
savef = savef + ".wav"
tts.save(savef)
# play mp3 audio
# mixer.music.load(savef)
# mixer.music.play()
# play wav audio
playsound('say2.wav')
rospy.sleep(2)
