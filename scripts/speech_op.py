#!/usr/bin/env python
'''
	>> installation <<
Google translate
	$ sudo -H pip install --ignore-installed six
	$ sudo -H pip install mock
	$ sudo pip install --upgrade google-cloud-translate
	$ sudo -H  pip install googletrans
	$ sudo -H pip install --upgrade google-api-python-client
	export GOOGLE_APPLICATION_CREDENTIALS=<path>
	
Google text to speech
	$ sudo -H pip install gTTS
MP3 audio play
	$ sudo -H pip install pygame

'''

import rospy, os, sys
import speech_recognition as sr
import std_msgs.msg 
from std_msgs.msg import String
from speech_jv.msg import target

import xml.etree.cElementTree as ET

from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

from google.cloud import translate
from gtts import gTTS
from pygame import mixer



# setting value
time_threshold = 0.5 # minimum at 0.5
duration = 4 # 1-3 

global j, state, speech, state_2_count, basic_q_n, x_input

x_input = "n"
j = 0
state = 0
state_2_count = 1
basic_q_n = 0
speech = ""
tree = ET.ElementTree(file='gpsr_af.xml')

# Record Audio
r = sr.Recognizer()
# The minimum length of silence (in seconds) that will register as the end of a phrase
r.pause_threshold = time_threshold # default is 0.8
 
# check device
for index, name in enumerate(sr.Microphone.list_microphone_names()):
	print ("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))
	if name == "default":
		device_index = index # select device

#set threhold level of noise
with sr.Microphone(device_index) as source:
	r.adjust_for_ambient_noise(source,duration)

print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
print ("Set minimum energy threshold to    {} ".format(r.energy_threshold)) # There is delay time "duration = 1" second as default
print ("Status of Dynamic_energy_threshold {} ".format(r.dynamic_energy_threshold))
print ("Pause time threshold 		{} ".format(r.pause_threshold))
print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Found \"default\" index = {}".format(device_index))

def callback(data):
	global x_input
	x_input = data.data

def target_input(q_,place_,object_,human_):
	tar_ = target()
	tar_.q = q_
	tar_.place = place_
	tar_.object = object_
	tar_.human = human_
	return tar_
	# pub_target.publish(tar_) 

def sleep(t):
	try:
		rospy.sleep(t)
	except:
		pass
def talk_out(ss):
	# s = soundhandle.voiceSound(ss)
	# s.repeat()
	# sleep(3)
	# s.stop()
	soundhandle.say(ss,"voice_don_diphone")
	sleep(3)

def talk_out_2(ss,t):
	soundhandle.say(ss,"voice_don_diphone")
	sleep(t)

def listen_jarvis():
	# listen from mic 
	with sr.Microphone(device_index) as source:
		print ("Jarvis is listening now {}".format(device_index))
		audio = r.listen(source)
	return audio 

def recognize_jarvis(audio):
	try:
		# recognize speech using Google Speech Recognition
		print ("wait wait wait")
		speech = r.recognize_google(audio,key = None, language = "en-US", show_all = False)
		speech = speech.lower()
		print ("jarvis listen : " + speech)
		return speech	
	except sr.UnknownValueError:
		print(">> {} << Google Speech Recognition could not understand audio".format(j))
		return "1"
	except sr.RequestError as e:
		print("Could not request results from Google Speech Recognition service; {0}".format(e))
		return "2"	

def recognize_jarvis_th(audio):
	try:
		# recognize speech using Google Speech Recognition
		print ("wait wait wait")
		speech = r.recognize_google(audio,key = None, language = "th-TH", show_all = False)
		# speech = speech.lower()
		print ("jarvis listen : " + speech)
		return speech	
	except sr.UnknownValueError:
		print(">> {} << Google Speech Recognition could not understand audio".format(j))
		return "1"
	except sr.RequestError as e:
		print("Could not request results from Google Speech Recognition service; {0}".format(e))
		return "2"	

def jarvis_get_command(speech):
	speech_l = speech.split()
	correct = 0 
	for jv in speech_l:
		if jv == "wake":
			correct = correct + 1
		elif jv == "up":
			correct = correct + 1
	if correct > 1:
		return 1
	else:
		return 0

def jarvis_get_command_okay(speech):
	speech_l = speech.split()
	correct = 0 
	for jv in speech_l:
		if jv == "yes":
			correct = correct + 1
			# index_jv = speech_l.index('jarvis')
			# speech = " ".join(speech_l[(index_jv+1):]) # get to know target
		elif jv == "okay":
			correct = correct + 1
		elif jv == "no":
			correct = -1
	if correct >= 1:
		return 1
	elif correct < 0:
		return -1
	else:
		return 0

def jarvis_get_stop(speech):
	speech_l = speech.split()
	correct = 0 
	for jv in speech_l:
		if jv == "stop":
			correct = 1
	if correct == 1:
		return 1
	else:
		return 0

def take_care(speech):
	speech_l = speech.split()
	correct = 0 
	for jv in speech_l:
		if jv == "take":
			correct = 1
		elif jv == "care":
			correct = 1
	if correct == 1:
		return 1
	else:
		return 0

def open_translate(speech):
	speech_l = speech.split()
	correct = 0 
	for jv in speech_l:
		if jv == "open":
			correct = 1
		elif jv == "mode":
			correct = 1
		elif jv == "translate":
			correct = 1
		elif jv == "translation":
			correct = 1
	if correct == 1:
		return 1
	else:
		return 0
def open_translate_select_mode(speech):
	speech_l = speech.split()
	correct = 0 
	language = 0
	for jv in speech_l:
		if jv == "open":
			correct = 1
		elif jv == "mode":
			correct = 1
		elif jv == "english":
			language = 1
		elif jv == "thai":
			language = 2
		elif jv == "language":
			language = 2
	if correct == 1 and language == 1:
		return 1
	elif correct == 1 and language == 2:
		return 2
	else:
		return 0

def close_translate(speech):
	speech_l = speech.split()
	correct = 0 
	for jv in speech_l:
		if jv == "close":
			correct = correct + 1
		elif jv == "mode":
			correct = correct + 1
		elif jv == "translation":
			correct = correct + 1
	if correct >= 1:
		return 1
	else:
		return 0

def close_translate_th(speech):
	ss_out = client.translate(speech,'en')
	text = ss_out['translatedText']
	text.lower()
	print text
	speech_l = text.split()
	correct = 0 
	for jv in speech_l:
		if jv == "close":
			correct = correct + 1
		elif jv == "shutdown":
			correct = correct + 1
		elif jv == "off":
			correct = correct + 1
		elif jv == "translation":
			correct = correct + 1
	if correct >= 1:
		return 1
	else:
		return 0

def translation_en_th(speech):
	# translate
	ss = speech.decode('utf-8')
	ss_out = client.translate(ss,'th')
	text = ss_out['translatedText']
	print text
	# gTTS
	# text_in = text.decode('utf-8')
	tts = gTTS(text, lang='th')
	tts.save("saythai.mp3")
	# play audio
	mixer.music.load('saythai.mp3')
	mixer.music.play()
	sleep(3)

def translation_th_en(speech):
	# translate
	# ss = speech.decode('utf-8')
	ss_out = client.translate(speech,'en')
	text = ss_out['translatedText']
	print text
	# gTTS
	# text_in = text.decode('utf-8')
	tts = gTTS(text, lang='en')
	tts.save("saythai.mp3")
	# play audio
	mixer.music.load('saythai.mp3')
	mixer.music.play()
	sleep(3)

def main_target():
	global speech, state, state_2_count, basic_q_n
	print "state >>>>>>>> ", state

	if state == 0:
		talk_out_2("i am ready",1)
		state = 1
	elif state == 1: # jarvis take care
		audio = listen_jarvis()
		speech = recognize_jarvis(audio)
		ok = take_care(speech)
		if ok == 1:
			talk_out_2("okay ser i will take care your friend",3)
			get_tar = target()
			get_tar = target_input(0,"wait15","","")
			pub_target.publish(get_tar)
			state = 2
		else:
			state = 1
	elif state == 2: # jarvis selection mode

		audio = listen_jarvis()
		speech = recognize_jarvis(audio)
		ok = open_translate(speech)
		if ok == 1 or x_input == "y":
			talk_out_2("i open the translation mode",2)
			get_tar = target()
			get_tar = target_input(0,"mode","","")
			pub_target.publish(get_tar)
			state = 3
		else:
			state = 2

	elif state == 3:
		audio = listen_jarvis()
		speech = recognize_jarvis(audio)
		ok = close_translate(speech)
		if ok == 1:
			talk_out_2("i close the translation mode",2)
			state = 4
		else:
			translation_en_th(speech)
			state = 3
	elif state == 4:
		audio = listen_jarvis()
		speech = recognize_jarvis(audio)
		ok = open_translate_select_mode(speech)
		if ok == 1: 	# en
			talk_out_2("i open the english translation mode",3)
			state = 3 	# back to 3
		elif ok == 2: 	# th
			talk_out_2("i open the thai language translation mode",3)
			state = 5	# have input th and close with th
		else: 
			state = 4
	elif state == 5:
		audio = listen_jarvis()
		speech = recognize_jarvis_th(audio)
		ok = close_translate_th(speech)
		if ok == 1:
			talk_out_2("i close the translation mode",2)
			state = 1
		else:
			translation_th_en(speech)
			state = 5

if __name__ == "__main__":
	try:
	# 	# init ros node
		rospy.init_node('speech_op', anonymous=True)
		client = translate.Client()
		mixer.init()
		soundhandle = SoundClient()
		rospy.sleep(1)
		soundhandle.stopAll() 
		
		pub_target = rospy.Publisher('Target', target, queue_size=5)
		rospy.Subscriber("enter_translation", String, callback)
		rate = rospy.Rate(5)
		while not rospy.is_shutdown():
			main_target()			
			rate.sleep()
				
	except rospy.ROSInterruptException:
		pass