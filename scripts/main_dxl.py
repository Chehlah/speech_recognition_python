#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Joy
from std_msgs.msg import Float64

global x_input
x_input = Joy()

def callback(data):
	global x_input
	x_input = data #input data from Joy 

def main():
	global x_input
	x_output = Float64()

	rospy.init_node('node_main', anonymous=True)

	rospy.Subscriber("joy", Joy, callback)
	pub = rospy.Publisher('/pan_controller/command', Float64, queue_size=10)

	rate = rospy.Rate(10) # 10hz
	while not rospy.is_shutdown():

		if len(x_input.axes) > 0: 
			if x_input.axes[1] == 1.0:
				x_output.data = 1.5
			elif x_input.axes[1] == -1.0:
				x_output.data = -1.5
			else:
				x_output.data = 0.0
		
		rospy.loginfo(x_input.axes)
		pub.publish(x_output)
		rate.sleep()

if __name__ == '__main__':
	try:
		main()
	except rospy.ROSInterruptException: pass