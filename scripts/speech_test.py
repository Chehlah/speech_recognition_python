import speech_recognition as sr

from google.cloud import translate
from gtts import gTTS
from pygame import mixer


client = translate.Client()
mixer.init()

# setting value
time_threshold = 0.8 # minimum at 0.5
duration = 3 # 

j = 0

# Record Audio
r = sr.Recognizer()
# The minimum length of silence (in seconds) that will register as the end of a phrase
r.pause_threshold = time_threshold # default is 0.8
 
# check device
for index, name in enumerate(sr.Microphone.list_microphone_names()):
	print ("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))
	if name == "default":
		device_index = index # select device

#set threhold level of noise
with sr.Microphone(device_index) as source:
	r.adjust_for_ambient_noise(source,duration)

print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
print ("Set minimum energy threshold to    	{} ".format(r.energy_threshold)) # There is delay time "duration = 1" second as default
print ("Status of Dynamic_energy_threshold 	{} ".format(r.dynamic_energy_threshold))
print ("Pause time threshold 				{} ".format(r.pause_threshold))
print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Found \"default\" index = {}".format(device_index))

if __name__ == "__main__":
	while 1:
		# listen from mic 
		with sr.Microphone(device_index) as source:
			print ("Jarvis is listening now {}".format(device_index))
			audio = r.listen(source)
		try:
			# recognize speech using Google Speech Recognition
			print ("wait wait wait")
			speech = r.recognize_google(audio,key = None, language = "en-US", show_all = False)# "en-US"
			# speech = speech.lower()
			print ("jarvis listen : " + speech)

			# # translate
			# # ss = speech.decode('utf-8')
			# ss_out = client.translate(speech,'th')
			# text = ss_out['translatedText']
			# print text
			# # gTTS
			# # text_in = text.decode('utf-8')
			# tts = gTTS(text, lang='th')
			# tts.save("saythai.mp3")
			# # play audio
			# mixer.music.load('saythai.mp3')
			# mixer.music.play()

				
		except sr.UnknownValueError:
			print(">> {} << Google Speech Recognition could not understand audio".format(j))
			
		except sr.RequestError as e:
			print("Could not request results from Google Speech Recognition service; {0}".format(e))
				