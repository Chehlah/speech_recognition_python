'''
	>> installation <<
Google translate
	$ sudo -H pip install --ignore-installed six
	$ sudo -H pip install mock
	$ sudo pip install --upgrade google-cloud-translate
	$ sudo -H  pip install googletrans
	$ sudo -H pip install --upgrade google-api-python-client
	export GOOGLE_APPLICATION_CREDENTIALS=<path>
Google text to speech
	$ sudo -H pip install gTTS
MP3 audio play
	$ sudo -H pip install pygame
'''

# -*-coding:utf-8-*-

from google.cloud import translate
from gtts import gTTS
from pygame import mixer

client = translate.Client()
mixer.init()

ss = "" # speech get string but in thai language

# >>>>>>>>>>>>>>>>>> en - th
# translate
ss = speech.decode('utf-8')
ss_out = client.translate(ss,'th')
text = ss_out['translatedText']
print text
# gTTS
# text_in = text.decode('utf-8')
tts = gTTS(text, lang='th')
tts.save("saythai.mp3")
# play audio
mixer.music.load('saythai.mp3')
mixer.music.play()

# >>>>>>>>>>>>>>>>>> th - en
# translate
# # ss = speech.decode('utf-8')
# ss_out = client.translate(speech,'en')
# text = ss_out['translatedText']
# print text
# # gTTS
# # text_in = text.decode('utf-8')
# tts = gTTS(text, lang='en')
# tts.save("saythai.mp3")
# # play audio
# mixer.music.load('saythai.mp3')
# mixer.music.play()