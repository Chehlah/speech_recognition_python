#!/usr/bin/env python
import rospy, os, sys
import speech_recognition as sr
import std_msgs.msg 
from std_msgs.msg import String
from speech_jv.msg import target
import xml.etree.cElementTree as ET
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

# setting value
time_threshold = 0.8 # minimum at 0.5
duration = 2 # 1-3 

global j, state, speech, state_2_count
j = 0
state = 0
state_2_count = 1
speech = ""
tree = ET.ElementTree(file='gpsr_af.xml')

# Record Audio
r = sr.Recognizer()
# The minimum length of silence (in seconds) that will register as the end of a phrase
r.pause_threshold = time_threshold # default is 0.8
 
# check device
for index, name in enumerate(sr.Microphone.list_microphone_names()):
	print ("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))
	if name == "default":
		device_index = index # select device

#set threhold level of noise
with sr.Microphone(device_index) as source:
	r.adjust_for_ambient_noise(source,duration)

print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
print ("Set minimum energy threshold to    {} ".format(r.energy_threshold)) # There is delay time "duration = 1" second as default
print ("Status of Dynamic_energy_threshold {} ".format(r.dynamic_energy_threshold))
print ("Pause time threshold 		{} ".format(r.pause_threshold))
print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Found \"default\" index = {}".format(device_index))		

def target_input(q_,place_,object_,human_):
	tar_ = target()
	tar_.q = q_
	tar_.place = place_
	tar_.object = object_
	tar_.human = human_
	return tar_
	# pub_target.publish(tar_) 

def sleep(t):
	try:
		rospy.sleep(t)
	except:
		pass
def talk_out(ss):
	# s = soundhandle.voiceSound(ss)
	# s.repeat()
	# sleep(3)
	# s.stop()
	soundhandle.say(ss,"voice_don_diphone")
	sleep(3)

def talk_out_2(ss,t):
	soundhandle.say(ss,"voice_don_diphone")
	sleep(t)

def listen_jarvis():
	# listen from mic 
	with sr.Microphone(device_index) as source:
		print ("Jarvis is listening now {}".format(device_index))
		audio = r.listen(source)
	return audio 

def recognize_jarvis(audio):
	try:
		# recognize speech using Google Speech Recognition
		print ("wait wait wait")
		speech = r.recognize_google(audio,key = None, language = "en-US", show_all = False)
		speech = speech.lower()
		print ("jarvis listen : " + speech)
		return speech	
	except sr.UnknownValueError:
		print(">> {} << Google Speech Recognition could not understand audio".format(j))
		return "1"
	except sr.RequestError as e:
		print("Could not request results from Google Speech Recognition service; {0}".format(e))
		return "2"	

def jarvis_get_command(speech):
	speech_l = speech.split()
	correct = 0 
	for jv in speech_l:
		if jv == "okay":
			correct = correct + 1
			# index_jv = speech_l.index('jarvis')
			# speech = " ".join(speech_l[(index_jv+1):]) # get to know target
		elif jv == "jarvis":
			correct = correct + 1
	if correct > 1:
		return 1
	else:
		return 0
def jarvis_get_command_okay(speech):
	speech_l = speech.split()
	correct = 0 
	for jv in speech_l:
		if jv == "yes":
			correct = correct + 1
			# index_jv = speech_l.index('jarvis')
			# speech = " ".join(speech_l[(index_jv+1):]) # get to know target
		elif jv == "okay":
			correct = correct + 1
		elif jv == "no":
			correct = -1
	if correct >= 1:
		return 1
	elif correct < 0:
		return -1
	else:
		return 0

def xml(speech):
	ss = speech.lower()
	ss = ss.split()
	
	# count tag start
	i_room = 0
	i_location = 0
	i_category = 0
	i_object = 0
	i_name  = 0

	room_l = []
	location_l = []
	category_l = []
	object_l = []
	name_l = []

	l = 0
	for w in ss:
		for elem in tree.iter('room'):
			try:
				if elem.attrib['name'] == w :
					room_l.append(w)
					i_room = i_room + 1
					break
				elif elem.attrib['name'] == w+" "+ss[l+1]:
					room_l.append(w+" "+ss[l+1])
					i_room = i_room + 1
					break
			except Exception as e:
				pass
		for elem in tree.iter('location'):
			try:
				if elem.attrib['name'] == w :
					location_l.append(w)
					i_location = i_location + 1
					break
				elif elem.attrib['name'] == w+" "+ss[l+1]:
					location_l.append(w+" "+ss[l+1])
					i_location = i_location + 1
					break
			except Exception as e:
				pass
		for elem in tree.iter('category'):
			try:
				if elem.attrib['name'] == w :
					category_l.append(w)
					i_category = i_category + 1
					break
				elif elem.attrib['name'] == w+" "+ss[l+1]:
					category_l.append(w+" "+ss[l+1])
					i_category = i_category + 1
					break
			except Exception as e:
				pass
		for elem in tree.iter('object'):
			try:
				if elem.attrib['name'] == w :
					object_l.append(w)
					i_object = i_object + 1
					break
				elif elem.attrib['name'] == w+" "+ss[l+1]:
					object_l.append(w+" "+ss[l+1])
					i_object = i_object + 1
					break
			except Exception as e:
				pass	
		for elem in tree.iter('name'):
			try:
				if elem.attrib['name'] == w :
					name_l.append(w)
					i_name = i_name + 1
					break
				elif elem.attrib['name'] == w+" "+ss[l+1]:
					name_l.append(w+" "+ss[l+1])
					i_name = i_name + 1
					break
			except Exception as e:
				pass	
		l = l+1
	print "i_room ",i_room
	print "i_location",i_location, location_l
	print "i_category",i_category
	print "i_object",i_object, object_l
	print "i_name",i_name
	# count tag end

	get_tar = target()
	#case
	if i_location == 1: 
		if i_object == 1:
			scase = 1
			for w in ss:
				if w == "how" or w == "many":
					scase = 2
				elif w == "find" or w == "locate" or w == "fine":
					scase = 3
			if scase == 1:
				for elem in tree.iter('object'):
					if elem.attrib['name'] == object_l[0]:
						location_l.append(elem.attrib['defaultLocation'])
				print ("publish",location_l[1],object_l[0],location_l[0])  # 1111
				get_tar = target_input(1,location_l[1],object_l[0],location_l[0])
				
			elif scase == 2:
				print ("publish",location_l[0],object_l[0],"many")     #4444
				get_tar = target_input(1,location_l[0],object_l[0],"many")
		
			elif scase == 3:
				print ("publish",location_l[0],object_l[0],"find")	#6666
				get_tar = target_input(1,location_l[0],object_l[0],"find")
				
		elif i_category == 1:
			print ("publish",location_l[0],category_l[0],"where")	#5555
			get_tar = target_input(1,location_l[0],object_l[0],"where")
			
	elif i_location == 2:
		if i_object == 1:
			if location_l[0] == "me":
				print ("publish",location_l[1],object_l[0],location_l[0])  #2222
				get_tar = target_input(1,location_l[1],object_l[0],location_l[0])
				
			else:
				print ("publish",location_l[0],object_l[0],location_l[1])  #3333
				get_tar = target_input(1,location_l[0],object_l[0],location_l[1])
	else:
		get_tar.q = target_input(0,"","","")
	return get_tar
				

def main_target():
	global speech, state, state_2_count
	print "state >>>>>>>> ", state
	
	if state == 0:
		talk_out("hello i am jarvis")
		state = 1
	
	elif state ==1: # chech start word "ok jarvis"
		audio = listen_jarvis()
		speech =  recognize_jarvis(audio)
		if speech == "2":
			talk_out("loss internet connection")
		ok = jarvis_get_command(speech)
		if ok == 1:
			talk_out("yes ser")
			state = 2
		else:
			state = 1

	elif state == 2: 
		talk_out_2("i am listening i am listening",3)
		audio = listen_jarvis()
		talk_out_2("i am thinking",2)
		speech =  recognize_jarvis(audio)
		if speech == "1":
			talk_out("i dont understand")
			# >>>>>>>>>>>>>>>>>>>>>> count 2 or 3 time for restart listening
			state_2_count = state_2_count + 1 # 1 2 3
			if state_2_count > 3:
				state = 1
				state_2_count = 1
			else:
				state = 2
		elif speech == "2":
			talk_out("loss internet connection")
		else:
			if speech != "":
				get_tar = target()
				get_tar = xml(speech)
				if get_tar.q == 1:
					speak = "Do you say do you say " + speech
					talk_out_2(speak,5)
					# talk_out_2(speak,5)
					state = 3
				else:
					speak = "maybe your request not correct or i can not do it"
					talk_out_2(speak,5)
					state = 2
			else:
				state = 1
	
	elif state == 3: 
		talk_out_2("yes or no yes or no",2)
		audio = listen_jarvis()
		talk_out_2("i am thinking",2)
		speech_2 = recognize_jarvis(audio)
		yes = jarvis_get_command_okay(speech_2)
		if yes == 1:
			# do the tast
			state = 4
		elif yes == -1: # no
			# listening again
			state = 2
		else:
			state = 3
			talk_out_2("yes or no yes or no",2)

	elif state == 4:
		print ("go go go")
		get_tar = target()
		get_tar = xml(speech)
		pub_target.publish(get_tar)
		speak = "i am going to do the tast"
		talk_out_2(speak,3)
		speak = "i am going to do the tast"
		talk_out_2(speak,3)
		state = 1

if __name__ == "__main__":
	try:
	# 	# init ros node
		rospy.init_node('speech_gpsr', anonymous=True)
		soundhandle = SoundClient()
		rospy.sleep(1)
		soundhandle.stopAll() 

		pub_target = rospy.Publisher('Target', target, queue_size=5)
		
		rate = rospy.Rate(5)
		while not rospy.is_shutdown():
			main_target()			
			rate.sleep()
				
	except rospy.ROSInterruptException:
		pass