#!/usr/bin/env python
import rospy, os, sys
import speech_recognition as sr
import std_msgs.msg 
from std_msgs.msg import String
from speech_jv.msg import target
import xml.etree.cElementTree as ET
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

# setting value
time_threshold = 0.8 # minimum at 0.5
duration = 2 # 1-3 

global j, state, speech, state_2_count, basic_q_n
j = 0
state = 0
state_2_count = 1
basic_q_n = 0
speech = ""
tree = ET.ElementTree(file='gpsr_af.xml')

# Record Audio
r = sr.Recognizer()
# The minimum length of silence (in seconds) that will register as the end of a phrase
r.pause_threshold = time_threshold # default is 0.8
 
# check device
for index, name in enumerate(sr.Microphone.list_microphone_names()):
	print ("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))
	if name == "default":
		device_index = index # select device

#set threhold level of noise
with sr.Microphone(device_index) as source:
	r.adjust_for_ambient_noise(source,duration)

print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
print ("Set minimum energy threshold to    {} ".format(r.energy_threshold)) # There is delay time "duration = 1" second as default
print ("Status of Dynamic_energy_threshold {} ".format(r.dynamic_energy_threshold))
print ("Pause time threshold 		{} ".format(r.pause_threshold))
print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Found \"default\" index = {}".format(device_index))

def target_input(q_,place_,object_,human_):
	tar_ = target()
	tar_.q = q_
	tar_.place = place_
	tar_.object = object_
	tar_.human = human_
	return tar_
	# pub_target.publish(tar_) 

def sleep(t):
	try:
		rospy.sleep(t)
	except:
		pass
def talk_out(ss):
	# s = soundhandle.voiceSound(ss)
	# s.repeat()
	# sleep(3)
	# s.stop()
	soundhandle.say(ss,"voice_don_diphone")
	sleep(3)

def talk_out_2(ss,t):
	soundhandle.say(ss,"voice_don_diphone")
	sleep(t)

def listen_jarvis():
	# listen from mic 
	with sr.Microphone(device_index) as source:
		print ("Jarvis is listening now {}".format(device_index))
		audio = r.listen(source)
	return audio 

def recognize_jarvis(audio):
	try:
		# recognize speech using Google Speech Recognition
		print ("wait wait wait")
		speech = r.recognize_google(audio,key = None, language = "en-US", show_all = False)
		speech = speech.lower()
		print ("jarvis listen : " + speech)
		return speech	
	except sr.UnknownValueError:
		print(">> {} << Google Speech Recognition could not understand audio".format(j))
		return "1"
	except sr.RequestError as e:
		print("Could not request results from Google Speech Recognition service; {0}".format(e))
		return "2"	

def jarvis_get_command(speech):
	speech_l = speech.split()
	correct = 0 
	for jv in speech_l:
		if jv == "okay":
			correct = correct + 1
			# index_jv = speech_l.index('jarvis')
			# speech = " ".join(speech_l[(index_jv+1):]) # get to know target
		elif jv == "jarvis":
			correct = correct + 1
	if correct > 1:
		return 1
	else:
		return 0
def jarvis_get_command_okay(speech):
	speech_l = speech.split()
	correct = 0 
	for jv in speech_l:
		if jv == "yes":
			correct = correct + 1
			# index_jv = speech_l.index('jarvis')
			# speech = " ".join(speech_l[(index_jv+1):]) # get to know target
		elif jv == "okay":
			correct = correct + 1
		elif jv == "no":
			correct = -1
	if correct >= 1:
		return 1
	elif correct < 0:
		return -1
	else:
		return 0

def next_game(speech):
	speech_l = speech.split()
	correct = 0 
	for jv in speech_l:
		if jv == "next":
			correct = correct + 1
			# index_jv = speech_l.index('jarvis')
			# speech = " ".join(speech_l[(index_jv+1):]) # get to know target
		elif jv == "game":
			correct = correct + 1
	if correct > 1:
		return 1
	else:
		return 0


def findAnswer():
	ss = speech.split()
	speech_q = ""
	speak = ""
	
	len_list = 0
	for elem in tree.iter('question'):
		len_list = len_list + 1
	list_count = [0] * len_list

	i = 0
	loop = 0
	for elem in tree.iter('question'):
		speech_q = elem.attrib['q']
		ss_q = speech_q.split()
		for w in ss:
			for w_q in ss_q:
				if w == w_q:
					list_count[i] = list_count[i] + 1
		loop = loop + 1
		if loop > 23:
			break
		else:
			if i <= 21:
				i = i + 1

	max_ = max(list_count)
	index_max = list_count.index(max_)
	print "max_ = ",max_," index = ",index_max	

	i = 0
	for elem in tree.iter('question'):
		if i == index_max:
			speak = elem.attrib['a'] # return answer
			print "The question is ",elem.attrib['q']
			print "The answer is ",elem.attrib['a']
		i = i+1

	return speak

def main_target():
	global speech, state, state_2_count, basic_q_n
	print "state >>>>>>>> ", state

	if state == 0:
		talk_out_2("hello i am okay jarvis",2)
		# state = 1
		# sent to main for turn 180 degree
		state = 7 

	elif state == 7: # start run for turn around
		audio = listen_jarvis()
		speech =  recognize_jarvis(audio)
		if speech == "2":
			talk_out_2("loss internet connection",3)
		ok = jarvis_get_command(speech)
		if ok == 1:
			talk_out_2("yes ser",1) # and send command
			get_tar = target()
			get_tar = target_input(0,"okay","","")
			pub_target.publish(get_tar)
			speak = "i turn back i turn back"
			talk_out_2(speak,3)
			state = 1
		else:
			state = 7
	
	elif state == 1: # wait amin count people and start play q game
		# speak = "call okay jarvis to talk with me"
		# talk_out_2(speak,3)
		audio = listen_jarvis()
		speech =  recognize_jarvis(audio)
		if speech == "2":
			talk_out_2("loss internet connection",3)
		ok = jarvis_get_command(speech)
		if ok == 1:
			talk_out_2("yes ser",1)
			state = 2
		else:
			state = 1

	elif state == 2:
		talk_out_2("i am listening your question now",3)
		audio = listen_jarvis()
		talk_out_2("i am thinking",2)
		speech =  recognize_jarvis(audio)
		if speech == "1":
			talk_out("i dont understand")
			# >>>>>>>>>>>>>>>>>>>>>> count 2 or 3 time for restart listening
			# state_2_count = state_2_count + 1 # 1 2 3
			# if state_2_count > 5:
			# 	state = 1
			# 	state_2_count = 1
			# else:
			# 	state = 2
			state = 2
		elif speech == "2":
			talk_out_2("loss internet connection",3)
			state = 2
		else:
			check_next_game = 0
			check_next_game  = next_game(speech)
			if check_next_game == 1:
				get_tar = target()
				get_tar = target_input(0,"5qa","","")
				pub_target.publish(get_tar)
				speak = "i am ready to next game"
				talk_out_2(speak,3)
				state = 1

			elif speech != "":
				speak = findAnswer()   	# check match question and get the answer of question
				if speak != "":	
					speak = "the answer is " + speak
					talk_out_2(speak,5)
					talk_out_2(speak,5) # could repeat
					basic_q_n = basic_q_n + 1
					state = 3
				else:
					speak = "maybe your request not correct or i can not do it"
					talk_out_2(speak,5)
					state = 2
			else:
				state = 2

	elif state == 3:
		if basic_q_n < 5:
			state = 2
			speak = "i am ready to next question"
			talk_out_2(speak,3)
		else:
			state = 1
	# elif state == 4:


if __name__ == "__main__":
	try:
	# 	# init ros node
		rospy.init_node('speech_spr', anonymous=True)
		soundhandle = SoundClient()
		rospy.sleep(1)
		soundhandle.stopAll() 

		pub_target = rospy.Publisher('Target', target, queue_size=5)
		
		rate = rospy.Rate(5)
		while not rospy.is_shutdown():
			main_target()			
			rate.sleep()
				
	except rospy.ROSInterruptException:
		pass