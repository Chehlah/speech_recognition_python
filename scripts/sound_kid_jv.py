#!/usr/bin/env python
'''
	>> installation <<
Sound play
	$ rosdep install sound_play
	$ rosmake sound_play
	$ sudo apt-get install libgstreamer-plugins-base1.0-dev
Add voice "voice_don_diphone"
	$ ls /usr/share/festival/voices/english
	$ sudo apt-get install festvox-don
Google text to speech
	$ sudo -H pip install gTTS
MP3 audio play
	$ sudo -H pip install pygame
Joy
	$ sudo apt-get install ros-kinetic-joy
WAV audio play
	$ sudo -H pip install playsound

Using joy	
	$ ls -l /dev/input/jsX
	$ rosparam set joy_node/dev "/dev/input/jsX"
	$ rosrun joy joy_node
Using soundplay_node
	$ rosrun sound_play soundplay_node.py
'''
import rospy
from sensor_msgs.msg import Joy

from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient

from gtts import gTTS
from pygame import mixer
from playsound import playsound

import xml.etree.cElementTree as ET

mixer.init()
tree = ET.ElementTree(file ='sound_kid_jv.xml')

global x_input
x_input = Joy()

def callback(data): #input data from Joy 
	global x_input
	x_input = data 

def sleep(t):
	try:
		rospy.sleep(t)
	except:
		pass

def talk_out(text,t):
	global soundhandle
	# # Comment when using direct sting
	# for elem in tree.iter(text):
	# 	text = elem.attrib['str']
	soundhandle.say(text,"voice_don_diphone")
	sleep(t)
	# soundhandle.say(ss,"voice_kal_diphone")
	# sleep(2)

def talk_out_th_online(savef,t):
	for elem in tree.iter(savef):
		text = elem.attrib['str']
	# gTTS
	# text_in = text.decode('utf-8')
	tts = gTTS(text, lang='th')
	savef = savef + ".mp3"
	tts.save(savef)
	# play audio
	mixer.music.load(savef)
	mixer.music.play()
	sleep(t)

def talk_out_th_offline(savef,t): # you need training sound with training_gTTS_th.py
	# savef = savef + ".mp3"
	# mixer.music.load(savef)
	# mixer.music.play()
	playsound(savef+".wav")
	sleep(t) 

def main():
	global x_input

	rate = rospy.Rate(10) # 10hz
	while not rospy.is_shutdown():

		# if state from Joy
		if len(x_input.axes) > 0: 
			if x_input.axes[1] == 1.0:
				talk_out_th_offline("say1",2)
			elif x_input.axes[1] == -1.0:
				talk_out_th_offline("say2",2)
			else:
				pass


		# # Sample sound jarvis w/ xml, output EN or TH
		# talk_out(text,2)
		# talk_out_th_online("say1",2)
		# talk_out_th_offline("say2",2)

		# # Sample with string EN
		# talk_out("sa wut dy krub",2) #thai karaoke
		# rospy.loginfo()
		rate.sleep()

if __name__ == '__main__':
	try:
		global soundhandle
		rospy.init_node('sound_kid_jv', anonymous=True)
		rospy.Subscriber("joy", Joy, callback)
		# soundhandle = SoundClient()
		# rospy.sleep(1)
		# soundhandle.stopAll() 
		main()
	except rospy.ROSInterruptException: pass
