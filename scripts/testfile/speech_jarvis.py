#!/usr/bin/env python
import rospy
import speech_recognition as sr
import std_msgs.msg 
from std_msgs.msg import String
import xml.etree.cElementTree as ET

# setting value
time_threshold = 0.5 # minimum at 0.5
duration = 1 # 1-3 

global j
j = 0
tree = ET.ElementTree(file='gpsr_jv.xml')

# Record Audio
r = sr.Recognizer()
# The minimum length of silence (in seconds) that will register as the end of a phrase
r.pause_threshold = time_threshold # default is 0.8
 
# check device
for index, name in enumerate(sr.Microphone.list_microphone_names()):
	print ("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))
	if name == "default":
		device_index = index # select device

#set threhold level of noise
with sr.Microphone(device_index) as source:
	r.adjust_for_ambient_noise(source,duration)

print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
print ("Set minimum energy threshold to    {} ".format(r.energy_threshold)) # There is delay time "duration = 1" second as default
print ("Status of Dynamic_energy_threshold {} ".format(r.dynamic_energy_threshold))
print ("Pause time threshold 		{} ".format(r.pause_threshold))
print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Found \"default\" index = {}".format(device_index))		

def speech_jarvis():
	global j
	
	# listen from mic 
	with sr.Microphone(device_index) as source:
		print ("Jarvis is listening now")
		audio = r.listen(source) 
		j = j + 1
	try:
		# recognize speech using Google Speech Recognition
		print ("wait wait wait")
		speech = r.recognize_google(audio,key = None, language = "en-US", show_all = False)
		# print(">> {} << You said: ".format(j) + speech)
		
	except sr.UnknownValueError:
		print(">> {} << Google Speech Recognition could not understand audio".format(j))
		return ("WW")
	except sr.RequestError as e:
		print("Could not request results from Google Speech Recognition service; {0}".format(e))
		return ("WW")

	speech = speech.lower()
	print ("Jarvis get command : {}".format(speech))
	speech_l = speech.split()
	speech = "" 
	for jv in speech_l:
		if jv == "jarvis":
			index_jv = speech_l.index('jarvis')
			speech = " ".join(speech_l[(index_jv+1):]) # get to know target
			break
	print ("Jarvis get command : {}".format(speech))

	ss = speech.split()
	q = 0
	for w_ in ss:	
		for elem in tree.iter('place'):
			if elem.attrib['name'] == w_:
				print ("publish {} {} {}".format(q,elem.tag,w_))
				q = q+1
				break		
		for elem in tree.iter('object'):
			if elem.attrib['name'] == w_:
				print ("publish {} {} {}".format(q,elem.tag,w_))
				q = q+1
				break
	speak = "Do you want jarvis " + speech
	if speech != "":
		pub_s.publish(speak)

if __name__ == "__main__":
	try:
		# init ros node
		rospy.init_node('speech_jarvis', anonymous=True)
		# pub_target = rospy.Publisher('Target', target, queue_size=5)
		pub_s = rospy.Publisher('jarvis_talk', String, queue_size=10)

		rate = rospy.Rate(10)
		while not rospy.is_shutdown():
			speech_jarvis()
			rate.sleep()
				
	except rospy.ROSInterruptException:
		pass