#!/usr/bin/env python
'''	
	Speech Recognition Library Reference
	>>open the microphone and start recording
		Microphone(device_index = None, sample_rate = 16000, chunk_size = 1024)
	>>list of the names of all available microphones
		Microphone.list_microphone_names()
	>>setting for sound input 
	energy_threshold 
	adjust_for_ambient_noise # use this for auto energy_threshold
	>>when listening
	pause_threshold = 0.8
	listen(source, timeout = None) # can input around 30 words/time but take more time.

'''
import rospy
import speech_recognition as sr
import std_msgs.msg
import re
from speech_jv.msg import target

'''
	simple msg:
		msg_ = target()
		msg_.place = "kitchen"
		msg_.objact = "coke" 
	must using Dynamic Reconfigure and Custom Messages:
		Combining C++/Python Publisher/Subscriber
			Python publisher -> C++ listeners 
			C++ publisher -> Python listener
'''
 
# setting value
global device_index, j, i_queue 
time_threshold = 0.7
j = 0
i_queue = 0

# Record Audio
r = sr.Recognizer()
 
# check device
for index, name in enumerate(sr.Microphone.list_microphone_names()):
	print ("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))
	if name == "default":#default
		device_index = index
	# if name == "USB  AUDIO: Audio (hw:1,0)":
	# 	device_index = index

#set threhold level of noise
with sr.Microphone(device_index) as source:
	r.adjust_for_ambient_noise(source,duration = 3)
	print("Set minimum energy threshold to {}".format(r.energy_threshold)) # There is delay time "duration = 1" second as default
print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Using Microphone index = {}".format(device_index))		

# The minimum length of silence (in seconds) that will register as the end of a phrase
r.pause_threshold = time_threshold

# Dictionary for command to Jarvis
command_place = ['[Kk]itchen']
command_object = ['[Cc]oke']
command_human = ['[Aa]min']


# input value to each target paramiter
def target_input(q_,place_,object_,human_):
	tar_ = target()
	tar_.q = q_
	tar_.place = place_
	tar_.object = object_
	tar_.human = human_
	return tar_ 

# publish to main_node
def pub_main(get_q):
	tar_out = target_input(get_q[0],get_q[1],get_q[2],get_q[3])
	pub_target.publish(tar_out)

# understanding what JV must do
def jv_listen(target_):
	global i_queue
	listen_target = "Jarvis listen : "
	get_target = []
	get_q = [0,'','','']

	for com_ in target_:
		if re.search(r"(?=("+'|'.join(command_place)+r"))",com_):
			get_ = re.findall(r"(?=("+'|'.join(command_place)+r"))",com_)
			get_target.append(get_[0].lower()) 
		elif re.search(r"(?=("+'|'.join(command_object)+r"))",com_):
			get_ = re.findall(r"(?=("+'|'.join(command_object)+r"))",com_)
			get_target.append(get_[0].lower()) 
		elif re.search(r"(?=("+'|'.join(command_human)+r"))",com_):
			get_ = re.findall(r"(?=("+'|'.join(command_human)+r"))",com_)
			get_target.append(get_[0].lower()) 


	if len(get_target) == 0:
		print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {} No command".format(listen_target))
	else:
		print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {} get command".format(listen_target))
		for l_ in get_target:
			print ("{} ".format(l_))

	for i in range(0,len(get_target)): # loop check type and put q 
		if re.search(r"(?=("+'|'.join(command_place)+r"))",get_target[i]):
			i_queue = i_queue + 1
			get_q = [i_queue,get_target[i],'no','no']
		elif re.search(r"(?=("+'|'.join(command_object)+r"))",get_target[i]):
			i_queue = i_queue + 1
			get_q = [i_queue,'no',get_target[i],'no']
		elif re.search(r"(?=("+'|'.join(command_human)+r"))",get_target[i]):
			i_queue = i_queue + 1
			get_q = [i_queue,'no','no',get_target[i]]
		pub_main(get_q)

	

# Speech recognition using Google Speech Recognition
def checkspeech(r):
	global device_index, j
	get_target = []

	with sr.Microphone(device_index) as source:
		audio = r.listen(source, timeout = None) 
		j = j + 1
	try:
		# recognize speech using Google Speech Recognition
		print(audio)
		speech = r.recognize_google(audio,key = None, language = "en-US", show_all = False)
		print(">> {} << You said: ".format(j) + speech)
		
	except sr.UnknownValueError:
		print(">> {} << Google Speech Recognition could not understand audio".format(j))
		return ("WW")
	except sr.RequestError as e:
		print("Could not request results from Google Speech Recognition service; {0}".format(e))
		return ("WW")

	# split string for easy to process 
	speech_l = speech.split()

	# Not using start word and get to know target
	jv_listen(speech_l)	

	# searching start keyword
	for jv in speech_l:
		if jv == "Friday":
			break
		else:
			jv = "Noooooooooooooooooooooo"
	if jv == "Friday":
		index_jv = speech_l.index('Friday')
		print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> Jarvis index : {}".format(index_jv))
		jv_listen(speech_l[(index_jv+1):]) # get to know target
	else:
		print (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> {}".format(jv))

	rate.sleep()

if __name__ == "__main__":
	try:
		# init ros node
		rospy.init_node('speech_google', anonymous=True)
		pub_target = rospy.Publisher('/Target', target, queue_size=5)

		rate = rospy.Rate(10) # 10hz
		while not rospy.is_shutdown():
			checkspeech(r)
			#rate.sleep()

	except rospy.ROSInterruptException:
		pass
	
