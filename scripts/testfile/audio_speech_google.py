#!/usr/bin/env python

''' 
	Edit: 13-09-2018
	auther: Aufa
	
	First time test topic /audio/audio from HSR to Google speech recognition
'''

import rospy
import speech_recognition as sr
import numpy as np
from audio_common_msgs.msg import AudioData

global dataArray, r, countData
dataArray = AudioData()
r = sr.Recognizer()
countData = 0

def callback(data):
	global dataArray, r, countData
	
	countData = countData + 1
	
	dataArray.data = dataArray.data + data.data

	if(countData > 100):
		# create new AudioData match with recognizer
		print ("get callback")
		countData = 0
		in_audio = sr.AudioData(dataArray.data,16000,2)
		dataArray = AudioData()
	
		try:
			# recognize speech using Google Speech Recognition
			speech = r.recognize_google(in_audio,key = None, language = "en-US", show_all = False)
			print(">>>> You said: "+ speech)
	
		except sr.UnknownValueError:
			print("Google Speech Recognition could not understand audio")
			return ("WW")
		except sr.RequestError as e:
			print("Could not request results from Google Speech Recognition service; {0}".format(e))
			return ("WW")

def listener():

	# init ros node
	rospy.init_node('audio_speech_google', anonymous=True)
	rospy.Subscriber("/audio/audio", AudioData, callback)
		
	rospy.spin()

if __name__ == '__main__':
    listener()

