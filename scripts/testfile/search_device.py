#!/usr/bin/env python
import rospy
import speech_recognition as sr

def check():
	for index, name in enumerate(sr.Microphone.list_microphone_names()):
		print ("Microphone with name \"{1}\" found for `Microphone(device_index={0})`".format(index, name))

if __name__ == "__main__":
	try:
		rospy.init_node('search_device', anonymous=True)
		check()
	except rospy.ROSInterruptException:
		pass