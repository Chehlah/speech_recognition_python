#!/usr/bin/env python

import rospy, os, sys
from sound_play.msg import SoundRequest

from sound_play.libsoundplay import SoundClient

def sleep(t):
    try:
        rospy.sleep(t)
    except:
        pass

if __name__ == '__main__':
    rospy.init_node('sound_jv', anonymous = True)
    soundhandle = SoundClient()

    rospy.sleep(1)
    
    soundhandle.stopAll()   

    print "This script will run continuously until you hit CTRL+C, testing various sound_node sound types."

    ss = "i am jarvis"
    s3 = soundhandle.voiceSound(ss)
    while 1:
        print "New API start voice"
        s3.repeat()
        sleep(3)

        print "New API stop"
        s3.stop()
