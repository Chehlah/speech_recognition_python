#!/usr/bin/env python
import rospy, os, sys
from sound_play.msg import SoundRequest
from sound_play.libsoundplay import SoundClient



def sleep(t):
    try:
        rospy.sleep(t)
    except:
        pass

def talker():

    rospy.init_node('custom_talker', anonymous=True)
    global soundhandle
	soundhandle = SoundClient()
	rospy.sleep(1)
	soundhandle.stopAll()   

    r = rospy.Rate(10) #10hz
    ss  =  "hello i am jarvis"
    while not rospy.is_shutdown():
        
        print ss
        s = soundhandle.voiceSound(ss)
        s.repeat()
        sleep(5)
        s.stop()  
            
        r.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException: pass