# ROS Google Speech Recognition

สั่งทำงาน node "speech_jv"  
เเต่สั่ง run ด้วย python ธรรมดาเท่านั้น เพราะ rospy ไม่รองรับ xml.etree.cElementTree   

โดยโปรเเกรมจะเปลี่ยน device_index ตามชื่อ name ที่เราเลือก  
สามารถเปลี่ยนค่า time_threshold ค่าไม่ต่ำกว่า 0.5  
***ต่อ Internet ด้วย  

### จากการทดสอบพบว่า

สามารถรับ Input ได้ประมาณ 30 words/time หรืออาจมากกว่า เเต่เวลารอ text จะนาน   
สามารถส่ง request ได้ไม่จำกัดโดยไม่ต้องขอ API   

### ความสามารถปัจจุบัน

1. speech_gpsr: General Purpose Service Robot  
2. speech_hmc: Help me carry  
3. speech_spr: Speech and Person Recognition  
4. speech_restaurant: Restaurant  
5. speech_op: Open Challenge  

### file ทดสอบต่างๆ 
1. xml_* : ใช้ทดสอบ algorithm string manipulation ร่วมกับ xml.etree.cElementTree  
2. speech_translation: ทดสอบความสามารถเเปลภาษา  
3. อื่นๆ เปิด code ดูเอง  


### Installation  

สำหรับ speech recognition เท่านั้น  
> git clone http://people.csail.mit.edu/hubert/git/pyaudio.git  
> sudo apt-get install portaudio19-*  
> cd pyaudio  
> sudo apt-get install python-dev  
> sudo python setup.py install  
> sudo apt-get install python-pip  
> sudo pip install SpeechRecognition  
> sudo pip install --upgrade pip  
  
สำหรับ soundplay  
> rosdep install sound_play  
> rosmake sound_play  
> sudo apt-get install libgstreamer-plugins-base1.0-dev  
  
ติดตั้งเสียงที่ใช้ใน code (voice_don_diphone)  
> ls /usr/share/festival/voices/english  
> sudo apt-get install festvox-don  
  
สำหรับ Google translation  
Google translate  
> sudo -H pip install --ignore-installed six  
> sudo -H pip install mock  
> sudo pip install --upgrade google-cloud-translate  
> sudo -H  pip install googletrans  
> sudo -H pip install --upgrade google-api-python-client  
  
export ไฟล์ .json (service account keys) ใน .bashrc *ควรขอใหม่เองเพราะมันฟรีเเละเลือกเปิดใช้เองได้ในอนาคต  
> export GOOGLE_APPLICATION_CREDENTIALS=<path>  
  
Google text to speech  
> sudo -H pip install gTTS  
  
MP3 audio play  
> sudo -H pip install pygame  



 

